# ansible playbook - linux
What it does:
- Pipeline configured to log into Ansible control host server and run a playbook against linux servers.

Setup:
- 192.168.0.60: Ansible control host (ubuntu)
- 192.168.0.62: shell runner "u62shell" (ubuntu)
- 192.168.0.63: target linux server (ubuntu)
- Ansible installed on control host (.60)
- "ansible" account configured on control host.
- ssh keys generated on control host for ansible acct.
- Public key copied to control host and target linux server authorized keys. 
- Private key added as $SSH_PRIVATE_KEY variable in project.

Sequence:
- Set permissions on private key.
- Copy files from repo to server via scp.
- Login to control host server.
- Run commands for debugging/verify basic os and id cmds work (for demo only).
- Run ansible ping module to verify inventory and connectivity.
- Run basic playbook to create a file on target linux server. 
